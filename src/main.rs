fn main() {
    let foo = "original string";
    let bar = foo;
    // bar = foo; cannot assign twice to immutable variable 
    // $ cargo check
    // Checking hello_cargo v0.1.0 (/home/level3/src/rust/hello_cargo)
    // warning: value assigned to `bar` is never read
    // --> src/main.rs:3:9
    // |
    // 3 |     let bar = foo;
    // |         ^^^
    // |
    // = note: #[warn(unused_assignments)] on by default
    // = help: maybe it is overwritten before being read?

    // error[E0384]: cannot assign twice to immutable variable `bar`
    // --> src/main.rs:4:5
    // |
    // 3 |     let bar = foo;
    // |         ---
    // |         |
    // |         first assignment to `bar`
    // |         help: make this binding mutable: `mut bar`
    // 4 |     bar = foo;
    // |     ^^^^^^^^^ cannot assign twice to immutable variable

    // error: aborting due to previous error

    // For more information about this error, try `rustc --explain E0384`.
    // error: Could not compile `hello_cargo`.

    // To learn more, run the command again with --verbose.
    println!("The value of foo is {}", foo);
    println!("The value of bar is {}", bar);
}